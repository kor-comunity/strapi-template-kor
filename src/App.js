import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import AppHeader from './containers/AppHeader';
import AppContainer from './containers/AppContainer';
import AppFooter from './containers/AppFooter';
import logoKor from './assets/images/kor-logo.svg';
import icon1 from './assets/images/tf1.svg'
import icon2 from './assets/images/tf2.svg'
import icon3 from './assets/images/tf3.svg'
import icon4 from './assets/images/tf4.svg'
import backgroundHome from './assets/images/background-home.svg';

require('dotenv').config();

function App() {
  const requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };
  
  const [ info, setInfo ]           = useState(null);
  const [ errorInfo, setErrorInfo ] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/landing-page`, requestOptions)
      .then(response => response.text())
      .then(result => setInfo(JSON.parse(result)))
      .catch(error => setErrorInfo('error', error));
  }, []);

  return (
    <>
     {info ? (
      <div className="app-container">
        <div className="app-header">
          <AppHeader
            logoKor={logoKor}
          />
        </div>

        <div className="app-content">
          <AppContainer
            info={info}
            icon1={icon1}
            icon2={icon2}
            icon3={icon3}
            icon4={icon4}
            errorInfo={errorInfo} 
            backgroundHome={backgroundHome}
          />
        </div>

        <div className="app-footer">
          <AppFooter />
        </div>
      </div>
     ) : (
      <div className="app-container">
        <p>{errorInfo}</p>
      </div>
     )}
    </>
  );
}

export default App;
