import React from 'react';
import { Form } from 'react-bootstrap';
import Button from 'react-bootstrap/Button'

const AppContainer = 
({ 
    info,
    icon1, 
    icon2, 
    icon3, 
    icon4,
    errorInfo, 
    backgroundHome, 
}) => {
    return (
        <div className="landing-container"   id="menu-home">
            <div className="welcome-page">
                <p>{info.title}</p>
                {info.headerImage[0].url && (
                    <img src={info.headerImage[0].url} alt="header"/>
                )}
            </div>

            <div className="how-it-works"  id="menu-feature">
                <div className="how-it-works-description">
                    <p>You can build your own app</p>
                    <p>{info.info1}</p>
                    <p>{info.title1}</p>
                    <Button variant="primary" className="button-get-started" >
                        Get Started
                    </Button>
                </div>

                <div className="sections-how-container">
                    <div className="section-1-how">
                        <div className="sub-section-how">
                            <img src={icon1} alt="icon1" />
                            <p>{info.info2}</p>
                            <p>{info.title2}</p>
                        </div>

                        <div className="sub-section-how">
                            <img src={icon2} alt="icon2" />
                            <p>{info.info3}</p>
                            <p>{info.title3}</p>
                        </div>
                    </div>

                    <div className="section-2-how">
                        <div className="sub-section-how">
                            <img src={icon3} alt="icon3" />
                            <p>{info.info4}</p>
                            <p>{info.title4}</p>
                        </div>

                        <div className="sub-section-how">
                            <img src={icon4} alt="icon4" />
                            <p>{info.info5}</p>
                            <p>{info.title5}</p>
                        </div>
                    </div>
                </div>


            </div>

            <div className="contact-form" id="menu-contact">
                <p>{info.footerTitle}</p>
                <p>{info.footerText}</p>
                <Form.Control
                    type="Text"
                    placeholder="Your mail"
                />

                <Button variant="primary" className="button-get-started" >
                    Send
                </Button>
            </div>
        </div>
    )
}

export default AppContainer;