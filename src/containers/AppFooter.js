import React from 'react';

const AppFooter = () => {
    return(
        <div className="footer-container">
            <div className="footer-sections">
                <div className="section-1">
                    <p>About Us</p>
                    <p>Support Center</p>
                    <p>Customer Support</p>
                    <p>About Us</p>
                    <p>Copyright</p>
                    <p>Popular Campaing</p>
                </div>

                <div className="section-2">
                    <p>Our Information</p>
                    <p>Return Policy</p>
                    <p>Privacy Policy</p>
                    <p>Terms & Conditions</p>
                    <p>Site Map</p>
                    <p>Store Hours</p>
                </div>

                <div className="section-3">
                    <p>My Account</p>
                    <p>Press Inquiries</p>
                    <p>Social Media Directories</p>
                    <p>Images & B-Roll</p>
                    <p>Permissions</p>
                    <p>Speakers Requests</p>
                </div>
            </div>
        </div>
    )
}

export default AppFooter;