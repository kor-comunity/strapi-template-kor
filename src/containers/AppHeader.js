import React from 'react';
import Button from 'react-bootstrap/Button'

const AppHeader = ({ logoKor }) => {

    const handleGoToSection = (sectionName) => {
        window.location.replace('/#'+sectionName)
    }

    return(
        <div className="header-menu">
            <div className="logo-kor">
                <img alt="logo-kor" src={logoKor} />
            </div>

            <div className="buttons-menu-1">
            <div className="menu-options">
                <p onClick={() => handleGoToSection('menu-home')}>Home</p>
                <p onClick={() => handleGoToSection('menu-feature')}>Feature</p>
                <p onClick={() => handleGoToSection('menu-contact')}>Contact Us</p>
            </div>

            <div className="button-get-started">
                <Button variant="primary" className="button-get-started" >
                    Get Started
                </Button>
            </div>
            </div>
        </div>
    )
}

export default AppHeader;